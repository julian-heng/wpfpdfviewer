﻿using System;
using System.Windows;

namespace WpfPdfViewer
{
    /// <summary>
    /// Interaction logic for InputBox.xaml
    /// </summary>
    public partial class InputBox : Window
    {
        public string Response => txtResponse.Text;

        public InputBox(string message, string defaultResponse = "")
        {
            InitializeComponent();

            lblMessage.Content = message;
            txtResponse.Text = defaultResponse;
        }

        private void BtnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            txtResponse.SelectAll();
            txtResponse.Focus();
        }
    }
}
