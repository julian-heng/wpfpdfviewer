﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace WpfPdfViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Debug.WriteLine("Closing Window");
            viewer.Close();
        }

        private void MenuOpen_Click(object sender, RoutedEventArgs e)
        {
            // Prompt user for the file
            var diag = new OpenFileDialog
            {
                Filter = "PDF Documents (.pdf)|*.pdf"
            };

            if (diag.ShowDialog() != true)
            {
                Debug.WriteLine("No file selected");
                viewer.Reset();
                return;
            }

            Debug.WriteLine($"Using filename \"{diag.FileName}\"");
            viewer.OpenFile(diag.FileName);
        }

        private void MenuSelect_Click(object sender, RoutedEventArgs e)
        {
            var sel = viewer.GetSelectedText();
            Debug.WriteLine($"'{sel}'");
        }

        private void MenuFind_Click(object sender, RoutedEventArgs e)
        {
            InputBox input = new InputBox("Enter query:");
            if (input.ShowDialog() != true)
            {
                Debug.WriteLine("No query given");
                return;
            }

            var query = input.Response;

            if (String.IsNullOrEmpty(query))
            {
                Debug.WriteLine("Query is an empty string");
                return;
            }

            viewer.Highlight(query);
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
