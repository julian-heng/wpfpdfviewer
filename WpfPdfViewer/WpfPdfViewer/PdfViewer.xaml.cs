﻿using CefSharp;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;

namespace WpfPdfViewer
{
    /// <summary>
    /// Interaction logic for PdfViewer.xaml
    /// </summary>
    public partial class PdfViewer : UserControl
    {
        private const string PDFJS_ADDRESS = "http://127.0.0.1:8000/web/viewer.html";
        private const string PDFJS_OPEN_SCRIPT = @"
            (function(data)
            {{
                var raw = atob(data);
                var arr = new Uint8Array(raw.length);
                for (var i = 0; i < raw.length; i++)
                {{
                    arr[i] = raw.charCodeAt(i);
                }}
                PDFViewerApplication.open(arr);
            }})('{0}');";

        private const string PDFJS_GET_TEXT_SCRIPT = @"window.getSelection().toString()";
        private const string PDFJS_FIND_SCRIPT = @"
            (function(query)
            {{
                PDFViewerApplication.findController.executeCommand('find',
                {{
                    query: query,
                    phraseSearch: true,
                    highlightAll: true
                }});
            }})('{0}')";

        private readonly SimpleHTTPServer server = new SimpleHTTPServer("pdfjs", 8000);

        public PdfViewer()
        {
            InitializeComponent();
            Reset();
        }

        public void Close()
        {
            Debug.WriteLine("Stopping server");
            this.server.Stop();
        }

        public void Reset()
        {
            browser.Address = PDFJS_ADDRESS;
        }

        public void OpenFile(string path)
        {
            // Base64 encode the file and load it using the
            // PDFViewerApplication.open method
            //
            // Done this way because we cannot load the file locally through
            // the url query (http://127.0.0.1:8000/web/viewer.html?file=file://path/to/pdf)
            // and would bring up errors about cross origin resource sharing.
            var b64 = Convert.ToBase64String(File.ReadAllBytes(path));
            browser.ExecuteScriptAsync(String.Format(PDFJS_OPEN_SCRIPT, b64));
        }

        public string GetSelectedText()
        {
            // Since pdf.js renders the pdf as html elements, we can just
            // invoke the window's get selection method
            var task = browser.EvaluateScriptAsync(PDFJS_GET_TEXT_SCRIPT);
            string sel = "";

            // Evaluating the script returns an async task, which we handle
            // here
            task.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    var response = t.Result;

                    if (response.Success && response.Result != null)
                    {
                        sel = (string)response.Result;
                    }
                }
            }).Wait();
            return sel;
        }

        public void Highlight(string query)
        {
            var task = browser.EvaluateScriptAsync(String.Format(PDFJS_FIND_SCRIPT, query));
            task.Wait();
        }
    }
}
